<!DOCTYPE html>
<html>
	<head>
		<title> AprilTopBrands| Cources </title>
		<!--/tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript">
			addEventListener("load", function () {
				setTimeout(hideURLbar, 0);
			}, false);

			function hideURLbar() {
				window.scrollTo(0, 1);
			}
		</script>
		<!--//tags -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
	</head>

	<body>
		<!-- header -->
			<?php include("header.php"); ?>
		<!-- header -->

		<div class="inner_page_agile">
			<h3> Available Courses </h3>
		</div>
		<!--//banner -->

		<!--/w3_short-->
			<div class="services-breadcrumb_w3layouts">
				<div class="inner_breadcrumb">

					<ul class="short_w3ls"_w3ls>
						<li><a href="index.html">Home</a><span>|</span></li>
						<li> Courses </li>
					</ul>
				</div>
			</div>
		<!--//w3_short-->

		<!-- /inner_content -->
			<div class="inner_content_info_agileits">
				<div class="container">
					<div class="tittle_head_w3ls">
						<h3 class="tittle">Our Courses</h3>
					</div>
					<div class="inner_sec_grids_info_w3ls">
						<div class="col-md-4 blog-grid one">
							<a href=""><img src="images/.jpg" alt=""></a>
							<div class="events_info">
								<h4><a href="">Developmental Sales Coaching Training</a></h4>
								<p>Lorem ipsum dolor sit amet,vehicula vel sapien et, feugiat sapien amet.</p>

							</div>
						</div>
						<div class="col-md-4 blog-grid one">
							<a href=""><img src="images/.jpg" alt=""></a>
							<div class="events_info">
								<h4><a href=""> Business Development Master Class </a></h4>
								<p>Lorem ipsum dolor sit amet,vehicula vel sapien et, feugiat sapien amet.</p>
							</div>
						</div>
						<div class="col-md-4 blog-grid one lost">
							<a href=""><img src="images/.jpg" alt=""></a>
							<div class="events_info">
								<h4><a href=""> Consultative Selling Training Program</a></h4>
								<p>Lorem ipsum dolor sit amet,vehicula vel sapien et, feugiat sapien amet.</p>
							</div>
						</div>
						<div class="col-md-4 blog-grid">
							<a href=""><img src="images/.jpg" alt=""></a>
							<div class="events_info">
								<h4><a href=""> Service Marketing Training </a></h4>
								<p>Lorem ipsum dolor sit amet,vehicula vel sapien et, feugiat sapien amet.</p>

							</div>
						</div>
						<div class="col-md-4 blog-grid">
							<a href=""><img src="images/.jpg" alt=""></a>
							<div class="events_info">
								<h4><a href="">Telemarketing Training</a></h4>
								<p>Lorem ipsum dolor sit amet,vehicula vel sapien et, feugiat sapien amet.</p>
							</div>
						</div>
						<div class="col-md-4 blog-grid lost">
							<a href=""><img src="images/.jpg" alt=""></a>
							<div class="events_info">
								<h4><a href="">Sales Training Program</a></h4>
								<p>Lorem ipsum dolor sit amet,vehicula vel sapien et, feugiat sapien amet.</p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!-- //inner_content -->

		<!-- footer -->
			<?php include("footer.php"); ?>
		<!-- //footer -->

		<!-- js -->
			<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
			<script type="text/javascript" src="js/bootstrap.js"></script>
		<!-- js -->
	</body>
</html>