<!DOCTYPE html>
<html>
	<head>
		<title> AprilTopBrands | Contact </title>
		<!--/tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript">
			addEventListener("load", function () {
				setTimeout(hideURLbar, 0);
			}, false);

			function hideURLbar() {
				window.scrollTo(0, 1);
			}
		</script>
		<!--//tags -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
	</head>

	<body>
		<!-- header -->
			<?php include("header.php"); ?>
		<!-- //header -->

		<!-- banner -->
			<div class="inner_page_agile">
				<h3>Contact Us</h3>
			</div>
		<!--//banner -->

		<!--/w3_short-->
			<div class="services-breadcrumb_w3layouts">
				<div class="inner_breadcrumb">
					<ul class="short_w3ls"_w3ls>
						<li><a href="index.html">Home</a><span>|</span></li>
						<li>Contact</li>
					</ul>
				</div>
			</div>
		<!--//w3_short-->

		<!-- /inner_content -->
			<div class="inner_content_info_agileits">
				<div class="container">
					<div class="tittle_head_w3ls">
						<h3 class="tittle">Contact Us</h3>
					</div>
					<div class="inner_sec_grids_info_w3ls">
						<div class="col-md-4 agile_info_mail_img_info">
							<div class="address-grid">
								<h4>Contact <span>Info</span></h4>
								<div class="mail-agileits-w3layouts">
									<i class="fa fa-volume-control-phone" aria-hidden="true"></i>
									<div class="contact-right">
										<p>Telephone </p><span>+234 (80)222-23-33</span>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="mail-agileits-w3layouts">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<div class="contact-right">
										<p>Mail </p><a href="mailto:info@example.com">info@example.com</a>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="mail-agileits-w3layouts">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
									<div class="contact-right">
										<p>Location</p><span>8088 USA, Honey block,New York City.</span>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="agileits_w3layouts_nav_right contact">
									<div class="social two">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-rss"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8 agile_info_mail_img">

						</div>
						<div class="clearfix"> </div>
						<div class="w3layouts_mail_grid">
							<form action="#" method="post">
								<div class="col-md-6 wthree_contact_left_grid">
									<input type="text" name="Name" placeholder="Name" required="">
									<input type="email" name="Email" placeholder="Email" required="">
									<input type="text" name="Telephone" placeholder="Telephone" required="">
									<input type="text" name="Subject" placeholder="Subject" required="">
								</div>
								<div class="col-md-6 wthree_contact_left_grid">
									<textarea name="Message" placeholder="Message..." required=""></textarea>
									<input type="submit" value="Submit">
								</div>
								<div class="clearfix"> </div>

							</form>
						</div>


					</div>

				</div>
			</div>
		<!-- //inner_content -->

		<!-- footer -->
			<?php include("footer.php"); ?>
		<!-- //footer -->

		<!-- js -->
			<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
			<script type="text/javascript" src="js/bootstrap.js"></script>
		<!-- js -->
	</body>
</html>