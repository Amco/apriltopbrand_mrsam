<!DOCTYPE html>
<html>
	<head>
		<title> AprilTopBrands | Home </title>
		<!--/tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript">
			addEventListener("load", function () {
				setTimeout(hideURLbar, 0);
			}, false);

			function hideURLbar() {
				window.scrollTo(0, 1);
			}
		</script>
		<!--//tags -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
	</head>

	<body>
		<!-- header -->
			<?php include("header.php"); ?>
		<!-- //header -->

		<!-- banner -->
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1" class=""></li>
					<li data-target="#myCarousel" data-slide-to="2" class=""></li>
					<li data-target="#myCarousel" data-slide-to="3" class=""></li>
				</ol>
				<div class="carousel-inner" role="listbox">
					<div class="item item1 active">
						<div class="container">
							<div class="carousel-caption">
								<h3>IT'S TIME TO SHOW UP DIFFERENTLY IN FRONT OF BUYERS</h3>
								<div class="agileits-button top_ban_agile">
								</div>
							</div>
						</div>
					</div>
					<div class="item item2">
						<div class="container">
							<div class="carousel-caption">
								<h3>Create the Sales Team that Drives <span> Your Number </span></h3>
								<div class="agileits-button top_ban_agile">
								</div>
							</div>
						</div>
					</div>
					<div class="item item3">
						<div class="container">
							<div class="carousel-caption">
								<h3>Sales Training Designed to Win With Every Buyer, <span>Every Time</span></h3>
								<div class="agileits-button top_ban_agile">
								</div>
							</div>
						</div>
					</div>
					<div class="item item4">
						<div class="container">
							<div class="carousel-caption">

								<h3> Your First Class Training <span>Provider</span></h3>
								<div class="agileits-button top_ban_agile">
								</div>
							</div>
						</div>
					</div>
				</div>
				<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					<span class="fa fa-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					<span class="fa fa-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				<!-- The Modal -->
			</div>
		<!--//banner -->

		<div class="banner-bottom">
			<div class="container">
				<div class="tittle_head_w3ls">
					<h3 class="tittle"> We Are AprilTopBrands </h3>
				</div>
				<div class="inner_sec_grids_info_w3ls">
					<div class="col-md-12 banner_bottom_left">
						<p style="text-align: justify;">
							Established in April 2010, April Topbrands Investment Ltd is an integrated experiential marketing agency offering specialist   services including; brand experience,  corporate communication, sales training,   sales activation; sales outsourcing, survey/ data analysis, events co-ordination. Our reputation as experts in the quality delivery of integrated communications services within approved schedules and budget. Our  strength lies in our professional knowledge and reputation for delivering excellent professional services.
						</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>

		<!-- //banner-bottom -->
		<div class="team_work_agile">
		</div>

		<!-- services -->
			<div class="services" id="services">
				<div class="container">
					<div class="tittle_head_w3ls">
						<h3 class="tittle">Courses</h3>
					</div>
					<div class="inner_sec_grids_info_w3ls">
						<div class="col-md-3 services-left">
							<div class="services-left-top">
								<h5>Sales Training Program</h5>
							</div>

							<div class="services-left-top services-left-top1">
								<h5> Business Development Master Class</h5>
							</div>
						</div>

						<div class="col-md-6 services-middle">
							<div class="services-middle-img">
								<img src="images/process.jpg" alt="" />
							</div>
							<div class="services-middle-grids">
								<div class="col-md-6 services-middle-left">
									<div class="services-left-top">
										<h5>Consultative Selling Training Program</h5>
									</div>
								</div>
								<div class="col-md-6 services-middle-left">
									<div class="services-left-top">
										<h5>Service Marketing Training</h5>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div class="col-md-3 services-left">
							<div class="services-left-top">
								<h5>Developmental Sales Coaching Training</h5>
							</div>
							<div class="services-left-top services-left-top1">
								<h5>Telemarketing Training</h5>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		<!-- //services -->

		<!-- /mid-services -->
			<div class="mid_services">
				<div class="col-md-6 according_inner_grids">
					<h3 class="agile_heading two">Why Choose Us</h3>
					<h2 style="margin-bottom: 20px; font-size: 15px; line-height: 22px; text-align: justify;">
						April Topbrands brings inspiration and innovations to every of our client business. We are highly experienced in every aspect of experiential marketing, sales management, survey and data administration and management, delivering this services in a most affordable and cost effective to our client.
						Through our activities, we deliver objective and well informed solutions to help our client grow . At APT, we take giant strides and innovative ways to offer our clients valued added solutions and perspectives to solving all their needs.
					</h2>
					<div class="according_info">
						<div class="panel-group about_panel" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title asd">
										<a class="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
										    aria-controls="collapseOne">
									  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>Our Mission
									</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body panel_text">
										Help Businesses make honest and efficient decisions.<br>
										We are thorough and collaborative<br>
										Integrity, honesty, openness, and balance.
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title asd">
										<a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
										    aria-controls="collapseTwo">
									  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>Our Vision
									</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body panel_text">
										To be a TOPBRAND for all your needs.
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6 mid_services_img">
				</div>
				<div class="clearfix"> </div>
			</div>
		<!-- //mid-services -->		

		<!-- footer -->
			<?php include("footer.php"); ?>
		<!-- //footer -->

		<!-- js -->
			<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
			<script type="text/javascript" src="js/bootstrap.js"></script>
		<!-- js -->
	</body>
</html>