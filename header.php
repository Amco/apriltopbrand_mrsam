<div class="header" id="home">
	<div class="content white agile-info">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">
						<img src="images/APT_png.png" class="img-fluid" height="40px">
					</a>
				</div>
				<!--/.navbar-header-->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<nav class="link-effect-2" id="link-effect-2">
							<ul class="nav navbar-nav">
								<li class="active"><a href="index.php" class="effect-3">About Us</a></li>
								<li class="dropdown">
									<a href="" class="dropdown-toggle effect-3" data-toggle="dropdown">Services <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="sales-training.php">Sales Training</a></li>
										<li class="divider"></li>
										<li><a href="activation.php">Sales Activation</a></li>
										<li class="divider"></li>
										<li><a href="outsourcing.php">Sales Outsourcing</a></li>
										<li class="divider"></li>
									</ul>
								</li>
								<li><a href="courses.php" class="effect-3">Courses</a></li>
								<li><a href="contact.php" class="effect-3">Contact</a></li>
							</ul>
						</nav>
					</div>
				<!--/.navbar-collapse-->
			</div>
		</nav>
	</div>
</div>