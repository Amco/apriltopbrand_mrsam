<!DOCTYPE html>
<html>
	<head>
		<title> AprilTopBrands | Sales Activation </title>
		<!--/tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript">
			addEventListener("load", function () {
				setTimeout(hideURLbar, 0);
			}, false);

			function hideURLbar() {
				window.scrollTo(0, 1);
			}
		</script>
		<!--//tags -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link href="css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
	</head>

	<body>
		<!-- header -->
			<?php include("header.php"); ?>
		<!-- //header -->

		<!-- slider -->
			<div class="">
			  	<div id="myCarousel" class="carousel slide" data-ride="carousel">
			    	<!-- Indicators -->
			    	<ol class="carousel-indicators">
				      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				      <li data-target="#myCarousel" data-slide-to="1"></li>
				      <li data-target="#myCarousel" data-slide-to="2"></li>
			    	</ol>

			    	<!-- Wrapper for slides -->
				    <div class="carousel-inner">
					    <div class="item activation_banner1 active">
					        <div class="carousel-caption" style="left: 0% !important;">
					          <h3>Sales Activation</h3>
					          <p>Sales Transformation for Improved Sales Effectiveness</p>
					        </div>
					    </div>

					    <div class="item activation_banner2">
					        <img src="chicago.jpg" alt="" style="width:100%;">
					        <div class="carousel-caption" style="left: 0% !important;">
					          <h3>Sales Activation</h3>
					          <p>Lorem ipsum dolor sit amet.</p>
					        </div>
					    </div>
				    
					    <div class="item activation_banner3">
					        <img src="ny.jpg" alt="" style="width:100%;">
					        <div class="carousel-caption" style="left: 0% !important;">
					          <h3>Sales Activation</h3>
					          <p>Lorem ipsum dolor sit amet.</p>
					        </div>
					    </div>	  
				    </div>
			  	</div>
			</div>
		<!--//slider -->

		<!-- about topbrands -->
			<div class="about_topbrands">
				<h2>We Are April Top Brands</h2>
				<p>
					A global <b>sales training company</b> focused on helping you drive revenue and grow long-term customer relationships. Our market-proven sales and coaching methodology combined with our active learning approach ensures that your sales teams learn, master, and apply new behaviors when and where they matter most — in front of the buyer. <br><br>

					Get to know us, and learn how we help drive the performance of the world’s most inspiring sales organizations to their next levels of excellence.
				</p>
			</div>
		<!-- end about topbrands -->

		<!-- banner at middle -->
			<div class="activation_middle_banner"></div>
		<!-- end banner at middle -->

		<!-- about topbrands -->
			<div class="about_topbrands_after_sales_middle_banner">
				<h2>Activation Training Designed to Win <br>With Every Buyer, Every Time</h2>
				<p>
					There is no second act in selling. Buyers have too many options and not enough time. When your salespeople show up throughout the sales cycle, they must be exceptional — cutting through the noise and distilling what matters most.

					That’s where we come in. We train your sales team to outperform the competition when the buyer has heard it all and is looking for substance.
				</p>
			</div>
		<!-- end about topbrands -->

		<!-- feature program -->
			<div class="about_topbrands_feature_program">
				<div class="about_topbrands_feature_program_content">
					<h2>Feature Program</h2>
				</div>

				<div class="container sales_container">
					<div class="row">
						<div class="col-md-3 col-sm-12">
							<div class="card" style="width: 100%;">
							  <div class="activation_first_img_inside_card"></div>
							  <div class="card-body">
							  	<h2>
							  		Activation Training
							  	</h2>
							    <p class="card-text">
							    	This training program uses a customer-focused approach to plan and execute sales calls that build trust and advance the sale. Apply a powerful road map and skills for successful needs-based dialogues that build credibility, enable better problem solving, foster customer openness, and arm you with critical information needed to position a compelling solution that drives customer value and differentiates you.
							    </p>
							  </div>
							</div>
						</div>

						<div class="col-md-3 col-sm-12">
							<div class="card" style="width: 100%;">
							  <div class="activation_second_img_inside_card"></div>
							  <div class="card-body">
							  	<h2>
							  		Activation Training
							  	</h2>
							    <p class="card-text">
							    	April Top Brand's High-Stakes Consultative Dialogues program is a collection of dialogue models that apply an advanced consultative approach, skills, and techniques to engage in higher-stakes dialogues needed to advance alignment, drive momentum, and win deals.
							    </p>
							  </div>
							</div>
						</div>

						<div class="col-md-3 col-sm-12">
							<div class="card" style="width: 100%;">
							  <div class="activation_third_img_inside_card"></div>
							  <div class="card-body">
							  	<h2>
							  		Activation Training
							  	</h2>
							    <p class="card-text">
							    	Using the Developmental Sales Coaching Framework and skills, sales managers learn to train team members to self-discover and self-assess ways to leverage strengths and continually grow and improve through effective problem solving.
							    </p>
							  </div>
							</div>
						</div>

						<div class="col-md-3 col-sm-12">
							<div class="card" style="width: 100%;">
							  <div class="activation_fourth_img_inside_card"></div>
							  <div class="card-body">
							  	<h2>
							  		Activation Training
							  	</h2>
							    <p class="card-text">
							    	Apply a strategic approach to planning and executing effective negotiations that align customer stakeholders, differentiate your offering and organization, preserve price and terms, and foster productive working relationships.
							    </p>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!-- end feature program -->

		<!-- banner at middle -->
			<div class="sec_activation_middle_banner"></div>
		<!-- end banner at middle -->

		<!-- about topbrands -->
			<div class="about_topbrands_after_sales_middle_banner">
				<h2>Create the Sales Team that Drives Your Number</h2>
				<p>
					The ground continues to shift under your sales team’s feet, and what worked in the past for your sales professionals and sales managers will no longer cut it. But, training your salespeople to show up differently in front of buyers can be a challenging task.<br><br>

					Get the benefit of our hindsight. As one of the most experienced sales training companies, we’ve done this before, and it’s our job to guide you to the most direct path to results. You’ll work with a team of sales experts who know what makes salespeople tick, what kind of training programs will get them to change their behavior in the field.

				</p>
			</div>
		<!-- end about topbrands -->

		<!-- footer -->
			<?php include("footer.php"); ?>
		<!-- //footer -->

		<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
		<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

		<script type="text/javascript" src="js/bootstrap.js"></script>
	</body>
</html>